PROGRAM THERM

IMPLICIT NONE

! ******************************************************
! Mesh

REAL, DIMENSION (:,:), ALLOCATABLE  :: Nodes,Nodes_orig
REAL, DIMENSION (:), ALLOCATABLE  :: v_mesh
REAL, DIMENSION (:), ALLOCATABLE  :: z_Length
INTEGER, DIMENSION (:,:), ALLOCATABLE  :: INDE,INDM,Bound01
INTEGER, DIMENSION (:,:), ALLOCATABLE  :: INDE_Mesh_Surf
INTEGER, DIMENSION (:), ALLOCATABLE  :: ICL,HICL,ICL_Evaporation,ID_Mesh_Surf
INTEGER, DIMENSION (1:6,1:4) :: FACES

INTEGER :: Nr_Elements,Nr_Nodes,Max_Zeilenlaenge,Nr_Elements_2D,Nr_Nodes_2D,Nr_Layers
INTEGER :: Nr_Nodes_Surface,Nr_Elements_Surface
INTEGER :: Nr_Nodes_Interface,Nr_Elements_Interface
INTEGER :: Nr_BoundaryConditions_01
INTEGER :: Dim_KrylovBase,Max_Nr_GMRES_Iterations
INTEGER :: Dim_KrylovBase_FS,Max_Nr_GMRES_Iterations_FS
INTEGER :: Nr_Evaporation

REAL :: xmin,ymin,zmin,xmax,ymax,zmax,zmax_orig,zmin_orig ! xy-Bereich, auf der die Quelle laufen kann f�r geo_fac=1
REAL :: geo_facx,geo_facy,geo_facz

! ******************************************************
! Mesh for free interface computing

REAL, DIMENSION (:,:), ALLOCATABLE  :: Nodes_Triangles,HNodes_Triangles
REAL, DIMENSION (:,:), ALLOCATABLE  :: Nodes_TripleLine,HNodes_TripleLine
INTEGER, DIMENSION (:,:), ALLOCATABLE  :: INDE_Triangles,Elements_per_Triangles

REAL :: Temp_Interface,xmin_TripleLine,xmax_TripleLine,ymin_TripleLine,ymax_TripleLine
INTEGER :: Nr_Elements_Triangles,Nr_Nodes_Triangles,Triangles_Length
INTEGER :: TripleLine_Length,Nr_TripleLine,Nr_Elements_per_Triangles

! ************************************
! Hilfsfelder f�r Interface
INTEGER, DIMENSION(1:8,1:3) :: BPN,BPNE  ! Brick Points Neighbours, Brick Points Neighbours Edges
INTEGER, DIMENSION(1:6,1:4) :: FACEP,FACEE
INTEGER, DIMENSION(1:12,1:2) :: EdgesP
INTEGER, DIMENSION(1:12,1:4) :: EdgesAE,Edge_Poly,Edge_NP

! ******************************************************
! Mesh for free surface computing

REAL, DIMENSION (:,:), ALLOCATABLE  :: Nodes1_FS,Nodes2_FS
INTEGER, DIMENSION (:,:), ALLOCATABLE  :: INDE_FS,INDM_FS
INTEGER, DIMENSION (:), ALLOCATABLE  :: ICL_FS,id_freesurf,id_back_freesurf

INTEGER :: Nr_Elements_FS,Nr_Nodes_FS,Max_Zeilenlaenge_FS

! ******************************************************
! Mesh for Maragoni flow computing

REAL, DIMENSION (:,:), ALLOCATABLE :: M11_Flow,M22_Flow,M33_Flow
REAL, DIMENSION (:), ALLOCATABLE :: MP_Flow,PR_Flow,DPR_Flow,PR_Nodes_Flow

REAL, DIMENSION (:), ALLOCATABLE :: UX_Flow,UY_Flow,UZ_Flow,VX_Flow,VY_Flow,VZ_Flow
REAL, DIMENSION (:), ALLOCATABLE :: WX_Flow,LWX_Flow,DG1_Flow,DG2_Flow,DG3_Flow
REAL, DIMENSION (:), ALLOCATABLE :: RES_Flow,LV_Flow,XG_Flow,LSX_Flow,LSY_Flow
REAL, DIMENSION (:,:), ALLOCATABLE :: VGM_Flow,HK_Flow
REAL, DIMENSION (:), ALLOCATABLE :: AP_Flow,AQ_Flow,YK_Flow,DK_Flow

REAL, DIMENSION (:,:), ALLOCATABLE  :: Nodes_Flow,Normal_Flow,Tan1_Flow,Tan2_Flow,Temp_Gradient_Flow
REAL, DIMENSION (:), ALLOCATABLE  :: Temp_Flow,sig_tan1_flow,sig_tan2_flow
INTEGER, DIMENSION (:,:), ALLOCATABLE  :: INDE_Flow,INDM_Flow
INTEGER, DIMENSION (:), ALLOCATABLE  :: ICL_Flow,id_Nodes_flow,id_Nodes_flow_back
INTEGER, DIMENSION (:), ALLOCATABLE  :: id_Elements_flow,Capillar_Points,id_Pressure_Nodes

REAL :: THETA,DT_Flow,DTC_Flow,FAC1_Flow,FAC2_Flow,TMin_Flow,Start_Residuum,TFixed_Flow
REAL :: UXMAX,UYMAX,UZMAX
INTEGER :: Nr_Elements_Flow,Nr_Nodes_Flow,Max_Zeilenlaenge_Flow
INTEGER :: Nr_Surf_Flow,Max_Nr_GMRES_Iterations_Flow,NrCapillar_Points,ICYCLE_Flow

! ******************************************************
! Matrix, Lastvektor und GMRES

REAL, DIMENSION (:,:), ALLOCATABLE :: Matrix
REAL, DIMENSION (:), ALLOCATABLE :: Lastvektor,RES,DG,XG
REAL, DIMENSION (:,:), ALLOCATABLE :: VGM,HK
REAL, DIMENSION (:), ALLOCATABLE :: AP,AQ,YK,DK
REAL, DIMENSION (:), ALLOCATABLE :: T0,T1
REAL, DIMENSION (:,:), ALLOCATABLE :: Temp_Gradient,Flow3D

! ******************************************************
! Matrix, Lastvektor und GMRES for free surface computing

REAL, DIMENSION (:,:), ALLOCATABLE :: Matrix_FS
REAL, DIMENSION (:), ALLOCATABLE :: Lastvektor_FS,RES_FS,DG_FS,XG_FS
REAL, DIMENSION (:,:), ALLOCATABLE :: VGM_FS,HK_FS
REAL, DIMENSION (:), ALLOCATABLE :: AP_FS,AQ_FS,YK_FS,DK_FS
REAL, DIMENSION (:), ALLOCATABLE :: TEMP_FS

! ******************************************************
! Materialeigenschaften

REAL, DIMENSION (:,:), ALLOCATABLE  :: cp_Table
REAL, DIMENSION (:,:), ALLOCATABLE  :: Density_Table
REAL, DIMENSION (:,:), ALLOCATABLE  :: lambda_Table,Vorfaktor

INTEGER :: Nr_Lambda, Nr_cp, Nr_Density
INTEGER :: Nr_Vorfaktor
REAL :: TLiquidus,TSolidus,LatentHeat,TEvaporation
REAL :: TMELT,nuMelt,Density_TM,FAC_CP_Function,DT_CP_Function

! ******************************************************
! Numerik Part

REAL, DIMENSION (1:8,1:3,1:3) :: IJAC
REAL, DIMENSION (1:8) :: DET
REAL, DIMENSION(1:3) :: CH
REAL, DIMENSION(1:2) :: ETA
REAL, DIMENSION (1:3) :: OM
REAL, DIMENSION (1:8,0:3,1:8) :: D_F
REAL, DIMENSION (1:8,1:3) :: RST
REAL, DIMENSION (1:4,0:2,1:3,1:3) :: FCT
REAL, DIMENSION (1:4,0:2,1:2,1:2) :: FBT
REAL :: PI
REAL :: D_TIME,TIME,D_TEMP,FAC1,FAC2,FAC3,EPSG,AL

REAL, DIMENSION (1:4,1:2,1:2) :: IJAC_FS
REAL, DIMENSION (1:4) :: DET_FS
REAL, DIMENSION (1:4,0:2,1:4) :: D_F_FS

REAL, DIMENSION (1:8,1:3,1:3) :: IJAC_Flow
REAL, DIMENSION (1:8) :: DET_Flow

! ******************************************************
! Leistungsdichteverteilung

INTEGER :: LDV_NX,LDV_NY,LDV_FACN
REAL, DIMENSION (:,:), ALLOCATABLE :: LDV
REAL, DIMENSION (:), ALLOCATABLE :: LDV_XP,LDV_YP
REAL, DIMENSION (:,:), ALLOCATABLE :: LDV_FAC,LDV_FACH
REAL, DIMENSION (:,:), ALLOCATABLE :: ILDV_FAC
REAL, DIMENSION (:), ALLOCATABLE :: LDV_XP_FAC,LDV_YP_FAC

! ******************************************************
! Programmverwaltung

INTEGER :: NCYCLE,ICYCLE,ICYCLE_FS,ICYCLE_EVAP,ICYCLE_NL,IMP,JMP,IDEC_FS,IDEC_Marangoni,Laser_Intensity 
REAL :: LDV_Position_X,LDV_Position_Y,LDV_Pos1_X,LDV_Pos1_Y,LDV_Pos2_X,LDV_Pos2_Y
REAL :: LDV_Io,LDV_ra,LDV_n,LDV_fn,LDV_ra_Limit,LDV_Pl
REAL :: emissivity,sigma_Boltzmann,DOPT
REAL :: VX,VY,VZ,TMIN,TMAX,v_mesh_max,Temp_Initial,LayerHeight
REAL :: XDUM_MP,PL_depth_fac,melt_depth
REAL :: Pressure,DVolume_surface,DVolume_mass,Verhaeltnis,element_height
character(20)::chr_time,name_list = "test.txt"
character(16)::chr_emi
character(16)::chr_n,chr_ra,chr_PL,chr_vx,chr_meshz,chr_element_height

! ******************************************************
Temp_Initial=30.E0
Triangles_Length=200000; ! Maximale L�nge der f�r die Interface bereit gestellten Dreiecke
TripleLine_Length=2000;

CALL Read_Material_Properties
geo_facx=1.0E0; geo_facy=1.0E0; geo_facz=1.50E0; !element_height=8E-3 !xy-Bereich, auf der die Quelle laufen kann, Meshfaktoren Standard: geo_facz=1.50E0
!geo_facx=6.E0*geo_facx; 
!geo_facy=6.E0*geo_facy; 
!geo_facz=0.5E0*geo_facz;

CALL Input_Mesh
CALL Initiate
CALL Derive_Mesh

! ******************************************************

EPSG=1.E-6
D_TIME=5.E-3;
AL=2.0E0/3.0E0;
FAC1=D_TIME*AL;
FAC2=D_TIME*(1.E0-AL);

! *******************************
! Leistungsdichteverteilung

namelist/parameter_list/ &
    
    
    & emissivity,  &            
    & sigma_Boltzmann, &
    & LDV_Io,         &
    & Laser_Intensity, &
    & PL_depth_fac,   &
    & LDV_ra,     &
    & LDV_n, &
    & LDV_ra_Limit,    &
    & vy,          &
    & vx

 open(unit = 10, file= name_list)
 
        read(10, nml = parameter_list)    
   
    close(10)
    
    print*, "Emissivity: ", emissivity             
    print*, "Sigma_Boltzmann: ", sigma_Boltzmann
    print*, "LDV_Io: ", LDV_Io
    print*, "Laser_Intensity: ", Laser_Intensity
    print*, "PL_depth_fac: ", PL_depth_fac
    print*, "LDV_ra: ", LDV_ra
    print*, "LDV_n: ", LDV_n
    print*, "LDV_ra_Limit: ", LDV_ra_Limit
    print*, "VY: ", vy 
    print*, "VX: ", vx
    
! W/mm^2/K^4
!--old

LDV_Pl=emissivity*Laser_Intensity!*195.E0; !Leistung*Emissions/Absorptionsfaktor


!0.040 !Strahradius in mm
!2=Gau� !15=TopHat
!0.10E0
LDV_fn=Calcfn(LDV_n);
PRINT *,' LDV_fn:',LDV_fn
LDV_Io=CALC_IoLDV(LDV_n,LDV_ra,LDV_ra_Limit,LDV_fn); 
PRINT *,' LDV_Io:',LDV_Io
PRINT *,' '
!stop

!Scangeschwindigkeit in mm/s

write(chr_vx,*) vx
write(chr_Pl,*) LDV_PL/emissivity
write(chr_meshz,*) geo_facz
write(chr_n,*) LDV_n
write(chr_ra,*) LDV_ra
write(chr_emi,*) emissivity
! ***************************

LDV_FACN=51;
ALLOCATE(LDV_FAC(0:LDV_FACN,0:LDV_FACN));
ALLOCATE(LDV_FACH(0:LDV_FACN,0:LDV_FACN));
ALLOCATE(ILDV_FAC(0:LDV_FACN,0:LDV_FACN));
ALLOCATE(LDV_XP_FAC(0:LDV_FACN));
ALLOCATE(LDV_YP_FAC(0:LDV_FACN));
do imp=0,LDV_FACN
 LDV_XP_FAC(imp)=-LDV_ra_Limit+2.E0*LDV_ra_Limit*REAL(imp)/REAL(LDV_FACN);
 LDV_YP_FAC(imp)=-LDV_ra_Limit+2.E0*LDV_ra_Limit*REAL(imp)/REAL(LDV_FACN);
END DO
LDV_FAC(0:LDV_FACN,0:LDV_FACN)=1.E0

! *******************************

!CALL READ_LDV


Flow3D(1:3,1:Nr_Nodes)=0.0E0;
Flow3D(1,1:Nr_Nodes)=vx;

LayerHeight=0.030E0
melt_depth=0.0

TIME=0.0E0

CALL Set_Interface ! besetzte Hilfsfelder
TMAX=25.0E0;

DO ICYCLE=1,250 !!Zeitschritte von bis
 
 D_TIME=((2.E0*LDV_ra)/VX)/100.E0
 if (icycle.eq. 1) D_TIME=D_TIME*0.025E0;
 if (icycle.eq. 2) D_TIME=D_TIME*0.050E0;
 if (icycle.eq. 3) D_TIME=D_TIME*0.075E0;
 if (icycle.eq. 4) D_TIME=D_TIME*0.100E0;
 if (icycle.eq. 5) D_TIME=D_TIME*0.200E0;
 if (icycle.eq. 6) D_TIME=D_TIME*0.300E0;
 if (icycle.eq. 7) D_TIME=D_TIME*0.400E0;
 if (icycle.eq. 8) D_TIME=D_TIME*0.500E0;
 if (icycle.eq. 9) D_TIME=D_TIME*0.600E0;
 if (icycle.eq.10) D_TIME=D_TIME*0.700E0;
 if (icycle.eq.11) D_TIME=D_TIME*0.800E0;
 if (icycle.eq.12) D_TIME=D_TIME*0.900E0;

 AL=2.0E0/3.0E0;
 FAC1=D_TIME*AL;
 FAC2=D_TIME*(1.E0-AL);

 LDV_Pos1_X=-0.0E0
 LDV_Pos1_y=0.0E0                        
 
 LDV_Pos2_X=-0.0E0+VX*D_TIME
 LDV_Pos2_y=0.0E0                        
  
 if (Nr_Elements_per_Triangles.eq.0) then 
  CALL Calc_Matrix_und_Lastvektor
  CALL FLUX_Iorn
  CALL GMRES
 end if
 
 if (TMAX.ge.TEvaporation+5.0E0) then
   do ICYCLE_EVAP=1,3 
     Temp_Interface=TEvaporation;
     CALL Calc_Interface;
     CALL Calc_Matrix_und_Lastvektor;
     CALL FLUX_Iorn;
     CALL FLUX_Interface;  
     CALL GMRES;
     if (TMAX.lt.TEvaporation+5.0E0) exit;
   end do
 end if 
 T0=T1;
  write(CHR_TIME, '(f)') time
 TIME=TIME+D_TIME
 if (melt_depth.gt.0.05) then
    PL_depth_fac=(melt_depth-0.05)/(0.175-0.05)+1.0
    if (Pl_depth_fac.gt.2) Pl_depth_fac=2.0
    print*,'increase absorption (depth): ',Pl_depth_fac
    Pl_depth_fac=1.0
    !stop
 end if

 !jonas: sp�ter auskommmentieren
 if (mod(icycle, 1).eq.0) then
     call calc_gradient
     call out
 end if
 !if (icycle.eq.179) then
     !call calc_gradient
     !call out
 !end if
 !if (icycle.eq.180) then
     !call calc_gradient
     !call out
 !end if

END DO

!CALL START('W');

CALL CALC_Gradient;
CALL OUT;

CONTAINS

INCLUDE "Model_Input.F90"
INCLUDE "T3D_Hex8.F90"
INCLUDE "Free_Surface_Quad4.F90"
INCLUDE "Interface.F90"
INCLUDE "FLOW_3D.F90"


! ***************************************************************
!                       Next Subroutine
! ***************************************************************


SUBROUTINE FLUX_Iorn

IMPLICIT NONE

REAL, DIMENSION (1:3,1:3) :: T_PKT,CP_PKT
REAL, DIMENSION (1:3,1:3) :: PXY,XDUM
REAL, DIMENSION (1:3) :: XR,XS,XN,XP
REAL :: SUM,RDUM,TEDUM,TEDUM4,TMedian,FRR,xpos,ypos,fac,xxdum,yydum,dxfac,ffx,ffy 

INTEGER, DIMENSION (1:4) :: IP
INTEGER :: i,j,k,ii,jj,kk,iz,it,ie,iface,i1,j1,idec

! **************************************************************

PRINT *,"  "
PRINT *," Start Subr. FLUX_Iorn"

FRR=exp(-LDV_fn*exp(LDV_n*log(LDV_ra_Limit/LDV_ra)));
PRINT *,'  FRR:',FRR

dxfac=abs(LDV_XP_FAC(2)-LDV_XP_FAC(1));

xpos=0.5E0*(LDV_Pos1_X+LDV_Pos2_X)
ypos=0.5E0*(LDV_Pos1_Y+LDV_Pos2_Y)

DO ii=1,Nr_BoundaryConditions_01

 ie=Bound01(1,ii);
 iface=Bound01(2,ii); 
 
 IP(1)=INDE(FACES(iface,1),ie);
 IP(2)=INDE(FACES(iface,2),ie);
 IP(3)=INDE(FACES(iface,3),ie);
 IP(4)=INDE(FACES(iface,4),ie);

 TMedian=T0(IP(1));
 if (TMedian.lt.T0(IP(2))) TMedian=T0(IP(2));
 if (TMedian.lt.T0(IP(3))) TMedian=T0(IP(3));
 if (TMedian.lt.T0(IP(4))) TMedian=T0(IP(4));

 if (TMedian.lt.TEvaporation) then
 do j=1,3
  do i=1,3

   XR(1:3)=0.0E0; XS(1:3)=0.0E0; XP(1:3)=0.0E0; TEDUM=0.0E0;

   DO jj=1,4
    iz=IP(jj)
    TEDUM=TEDUM+T0(iz)*FCT(jj,0,i,j)
    XP(1:3)=XP(1:3)+Nodes(1:3,iz)*FCT(jj,0,i,j)
    XR(1:3)=XR(1:3)+Nodes(1:3,iz)*FCT(jj,1,i,j)
    XS(1:3)=XS(1:3)+Nodes(1:3,iz)*FCT(jj,2,i,j)
   END DO

   !idec=0;
   !if (Nr_TripleLine.gt.0) then
   !  idec=WithinTripleLine(XP(1),XP(2));
   !end if
   !if (TEDUM.gt.TEvaporation) idec=1;
   
   TEDUM4=0.0E0
   if (TEDUM.gt.Temp_Initial) then
    rdum=TEDUM+273.15E0 
    TEDUM4=rdum*rdum*rdum*rdum
   end if

   XN(1)=(XR(2)*XS(3)-XR(3)*XS(2))
   XN(2)=(XR(3)*XS(1)-XR(1)*XS(3))
   XN(3)=(XR(1)*XS(2)-XR(2)*XS(1))

   SUM=SQRT(XN(1)**2+XN(2)**2+XN(3)**2)
   XN(1)=XN(1)/SUM
   XN(2)=XN(2)/SUM
   XN(3)=XN(3)/SUM

   XDUM(i,j)=SUM !*ABS(XN(3))

   PXY(i,j)=0.0E0
   
   RDUM=SQRT((XP(1)-LDV_Pos1_X)**2+(XP(2)-LDV_Pos1_Y)**2);
   if (RDUM<0.00001E0) RDUM=0.00001E0;
   if (RDUM.LE.LDV_ra_Limit) then    
     PXY(i,j)=FAC1*(Pl_depth_fac*LDV_PL*ABS(XN(3))*LDV_Io*(exp(-LDV_fn*exp(LDV_n*log(RDUM/LDV_ra)))-FRR) &
                       -emissivity*sigma_Boltzmann*TEDUM4);
   end if;   
   
   RDUM=SQRT((XP(1)-LDV_Pos2_X)**2+(XP(2)-LDV_Pos2_Y)**2);
   if (RDUM<0.00001E0) RDUM=0.00001E0;
   if (RDUM.LE.LDV_ra_Limit) then    
     PXY(i,j)=PXY(i,j)+FAC2*(Pl_depth_fac*LDV_PL*ABS(XN(3))*LDV_Io*(exp(-LDV_fn*exp(LDV_n*log(RDUM/LDV_ra)))-FRR) &
                                -emissivity*sigma_Boltzmann*TEDUM4);
   end if;
   !if (idec.eq.1) PXY(i,j)=0.0E0 
  end do
 end do

 DO it=1,4
  iz=IP(it)
  SUM=0.0E0;
  DO j=1,3
   DO i=1,3
    SUM=SUM+OM(i)*OM(j)*FCT(it,0,i,j)*PXY(i,j)*XDUM(i,j)
   END DO
  END DO
  Lastvektor(iz)=Lastvektor(iz)+SUM
 END DO

 end if
END DO

PRINT *," Ende  Subr. FLUX_Iorn"
PRINT *,"  "

END SUBROUTINE FLUX_Iorn


! ***************************************************************
!                       Next FUNCTION
! ***************************************************************


FUNCTION CALC_IoLDV (ne,ra,ra_limit,fn) RESULT(CALC_IoLDV_VALUE)

IMPLICIT NONE

REAL :: ne,ra,ra_limit,fn,CALC_IoLDV_VALUE
REAL :: sum,xdum,frr,dr,rr
INTEGER :: ii,jj,nn

  nn=1000;
  dr=ra_limit/REAL(nn);
  frr=exp(-fn*exp(ne*log(ra_limit/ra)));

  sum=0.0E0;
  do ii=1,nn 
   rr=(REAL(ii)-0.5E0)*dr;
   sum=sum+(exp(-fn*exp(ne*log(rr/ra)))-frr)*rr;
  end do;
  sum=sum*2.0E0*3.141592654E0*dr;
  
  CALC_IoLDV_VALUE=1.0E0/sum;

END FUNCTION CALC_IoLDV


END PROGRAM THERM