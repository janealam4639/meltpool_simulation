PROGRAM MESHV

! Programm liest Daten (Knotenpunkte, Zusammengehoerigkeit der
! Dreiecke) ein und konstruiert daraus ein der Phasengrenze an-
! gepasstes FE-Netz.

IMPLICIT NONE

INTEGER, PARAMETER :: NKD=4712, NID=2356
INTEGER, PARAMETER :: N1=20,NX=10,N2=2*NX,N3=12,N4=20
INTEGER, PARAMETER :: NK=(N1+1)*(N4+N2+N4+1)*(N3+1)+ &
                         (N1+1)*(N2-1)*N4+(N2-1)*N3*N4
INTEGER, PARAMETER :: NE=N1*(N4+N2+N4)*N3+N1*N2*N4+N2*N3*N4
INTEGER, PARAMETER :: NESU=N1*(N4+N2+N4)+N2*N4
INTEGER, PARAMETER :: NESL=N3*(2*N1+N2)+N1*N2
INTEGER, PARAMETER :: NS =(N1+1)*(N4+N2+N4+1)+(N2-1)*N4
INTEGER, PARAMETER :: NSL=2*(N3+1)*(N1+1)+(N3+1)*(N2-1)+(N2-1)*N1
INTEGER, PARAMETER :: NSY=(N3+1)*(N4+N2+N4+1)+(N2-1)*N4

REAL, DIMENSION (1:3,1:NKD) :: KND
REAL, DIMENSION (1:2*NX+1) :: XP
REAL, DIMENSION (1:N1+1  ) :: YP
REAL, DIMENSION (1:N3+1  ) :: ZP

REAL :: FX,FY,FZ

INTEGER, DIMENSION (1:3,1:NID) :: DINDE

! BL1, BL2, BL3 speichert die Information der Knotennummern
! im logischen Raum nach Skriptvorgabe

INTEGER, DIMENSION (1:N4+N2+N4+1,1:N1+1,1:N3+1) :: BL1
INTEGER, DIMENSION (1:N2+1,1:N1+1,1:N4+1) :: BL2
INTEGER, DIMENSION (1:N2+1,1:N4+1,1:N3+1) :: BL3

REAL, DIMENSION (1:3,1:NK) :: KN
INTEGER, DIMENSION (1:8,1:NE) :: INDE
INTEGER, DIMENSION (1:NESU) :: IESU
INTEGER, DIMENSION (1:2,1:NESL) :: IESL
INTEGER, DIMENSION (1:4,1:NESU) :: INDESU
INTEGER, DIMENSION (1:NS) :: NRS
INTEGER, DIMENSION (1:NSL) :: NRSL
INTEGER, DIMENSION (1:NSY) :: NRSY

REAL, DIMENSION (1:3,1:9) :: KW
INTEGER :: IDUM

! **************************************************************

CALL DATIN
CALL INDEX
CALL MESH
CALL OUT

PRINT *," NK  = ",NK
PRINT *," NE  = ",NE
PRINT *," NESU= ",NESU
PRINT *," NS = ",NS
PRINT *," NSL= ",NSL
PRINT *," NSY= ",NSY

CONTAINS


  SUBROUTINE OUT

  IMPLICIT NONE

  INTEGER :: I,J

  PRINT *,"  "
  PRINT *," Start Subr. OUT"

  OPEN(1,FILE='VMESH.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) ((KN(J,I),J=1,3),I=1,NK)
  CLOSE(1)

  OPEN(1,FILE='VINDE.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) ((INDE(J,I),J=1,8),I=1,NE)
  CLOSE(1)

  OPEN(1,FILE='IESU.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) (IESU(I),I=1,NESU)
  CLOSE(1)

  OPEN(1,FILE='IESL.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) ((IESL(I,J),I=1,2),J=1,NESL)
  CLOSE(1)

  OPEN(1,FILE='NRSL.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) (NRSL(I),I=1,NSL)
  CLOSE(1)

  OPEN(1,FILE='NRS.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) (NRS(I),I=1,NS)
  CLOSE(1)

  OPEN(1,FILE='NRSY.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
   WRITE(1) (NRSY(I),I=1,NSY)
  CLOSE(1)

  PRINT *,"  "
  PRINT *," End Subr. OUT"
  PRINT *,"  "

END SUBROUTINE OUT



SUBROUTINE SUB_01(ID,IDEC,P0,PT,XS)

! fuehrt Newton Iteration mit dem Ziel der Bestimmung des Schnitt-
!                punktes eine Ebene mit Strahl durch
! ****************************************************************

IMPLICIT NONE

INTEGER :: I,ID,IT,IDEC

DOUBLE PRECISION :: R,S,T,DET,XDUM
REAL, DIMENSION (3) :: P0,PT,XS
DOUBLE PRECISION, DIMENSION (3) :: DP0,DPT,P1,P2,P3,AV,BV,CV,FV,XH
DOUBLE PRECISION, DIMENSION (3,3) :: AM,BM

 IDEC=0

 DO I=1,3
  DP0(I)=DBLE(P0(I))
  DPT(I)=DBLE(PT(I))

  P1(I)=DBLE(KND(I,DINDE(1,ID)))
  P2(I)=DBLE(KND(I,DINDE(2,ID)))
  P3(I)=DBLE(KND(I,DINDE(3,ID)))
 END DO

 AV=P2-P1; BV=P3-P1; CV=DPT

 XH(1)=AV(2)*BV(3)-AV(3)*BV(2)
 XH(2)=AV(3)*BV(1)-AV(1)*BV(3)
 XH(3)=AV(1)*BV(2)-AV(2)*BV(1)

 XDUM=XH(1)*XH(1)+XH(2)*XH(2)+XH(3)*XH(3)
 XDUM=DSQRT(XDUM)
 IF (XDUM.EQ.0.0D0) THEN
  PRINT *,'Kreuzprodukt gleich Null!'
  STOP
 END IF
 DO I=1,3
  XH(I)=XH(I)/XDUM
 END DO

 XDUM=DPT(1)*DPT(1)+DPT(2)*DPT(2)+DPT(3)*DPT(3)
 XDUM=DSQRT(XDUM)
 IF (XDUM.EQ.0.0D0) THEN
  PRINT *,'Richtungsvektor gleich Null!'
  STOP
 END IF
 DO I=1,3
  DPT(I)=DPT(I)/XDUM
 END DO

 XDUM=XH(1)*DPT(1)+XH(2)*DPT(2)+XH(3)*DPT(3)

 IF (DABS(XDUM).GT.1.D-3) THEN

 R=0.5D0;  S=0.5D0;  T=0.5D0;

 DO IT=1,5

  IF (R.EQ.0.D0) R=1.D-6
  IF (S.EQ.0.D0) S=1.D-6

  DO I=1,3
   AM(I,1)=AV(I)+S*(BV(I)-AV(I))
   AM(I,2)=      R*(BV(I)-AV(I))
   AM(I,3)=-CV(I)
  END DO

! Invertierungsalgorithmus fuer 3*3 Matrix
! ****************************************

  DET = AM(1,1)*AM(2,2)*AM(3,3) &
       +AM(2,1)*AM(3,2)*AM(1,3) &
       +AM(3,1)*AM(1,2)*AM(2,3) &
       -AM(3,1)*AM(2,2)*AM(1,3) &
       -AM(1,1)*AM(3,2)*AM(2,3) &
       -AM(2,1)*AM(1,2)*AM(3,3)

  IF (DET.EQ.0.0D0) THEN
   PRINT *,'Determinante gleich Null!'
   EXIT
   !STOP
  END IF

  BM(1,1) = (AM(2,2)*AM(3,3)-AM(2,3)*AM(3,2))/DET
  BM(1,2) = (AM(3,2)*AM(1,3)-AM(3,3)*AM(1,2))/DET
  BM(1,3) = (AM(1,2)*AM(2,3)-AM(1,3)*AM(2,2))/DET

  BM(2,1) = (AM(2,3)*AM(3,1)-AM(2,1)*AM(3,3))/DET
  BM(2,2) = (AM(3,3)*AM(1,1)-AM(3,1)*AM(1,3))/DET
  BM(2,3) = (AM(1,3)*AM(2,1)-AM(1,1)*AM(2,3))/DET

  BM(3,1) = (AM(2,1)*AM(3,2)-AM(2,2)*AM(3,1))/DET
  BM(3,2) = (AM(3,1)*AM(1,2)-AM(3,2)*AM(1,1))/DET
  BM(3,3) = (AM(1,1)*AM(2,2)-AM(1,2)*AM(2,1))/DET

  FV(1)=P1(1)-P0(1)+R*AV(1)+R*S*(BV(1)-AV(1))-T*CV(1)
  FV(2)=P1(2)-P0(2)+R*AV(2)+R*S*(BV(2)-AV(2))-T*CV(2)
  FV(3)=P1(3)-P0(3)+R*AV(3)+R*S*(BV(3)-AV(3))-T*CV(3)

  R=R-BM(1,1)*FV(1)-BM(1,2)*FV(2)-BM(1,3)*FV(3)
  S=S-BM(2,1)*FV(1)-BM(2,2)*FV(2)-BM(2,3)*FV(3)
  T=T-BM(3,1)*FV(1)-BM(3,2)*FV(2)-BM(3,3)*FV(3)

 END DO

  IF ((R.GE.-1.D-6).AND.(R.LE.1.000001D0).AND. &
      (S.GE.-1.D-6).AND.(S.LE.1.000001D0).AND. &
      (T.GT.0.D0)) THEN
   IDEC=1
   DO I=1,3
    XS(I)=REAL(P0(I)+T*CV(I))
   END DO

   XDUM=DSQRT(FV(1)*FV(1)+FV(2)*FV(2)+FV(3)*FV(3))

   IF (XDUM.GT.1.D-6) THEN
    PRINT *,"ID, XDUM", ID,XDUM
   END IF
  END IF

  END IF

END SUBROUTINE SUB_01



  SUBROUTINE MESH

  IMPLICIT NONE

  REAL, DIMENSION(1:3,1:N2+1,1:N1+1) :: SB1,SB2
  REAL, DIMENSION(1:3,1:N2+1,1:N3+1) :: SS1,SS2
  REAL, DIMENSION(1:3,1:N1+1,1:N3+1) :: SL1,SL2

  REAL, DIMENSION(1:3) :: X1,X2,X3,X4,P0,PT,XS,XDV
  REAL :: DXX,DYY,DZZ,R,S,T
  INTEGER :: I,J,K,IK,IE,ID,IDEC
  INTEGER :: I1,I2,I3,I4,I5,I6,I7,I8

  PRINT *,"  "
  PRINT *," Start Subr. MESH"

! ***************************************

! BLOCK 1
  PRINT *,'Block 1'
  IK=0
  P0(1:3)=KW(1:3,9)

  DO K=1,N3+1
   DO J=1,N1+1

!    IF (YP(J).LT.1.0E0) P0(2)=YP(J)
!    IF (YP(J).GE.1.0E0) P0(2)=1.0E0

     X2(1:3)=KW(1:3,5)+YP(J)*(KW(1:3,8)-KW(1:3,5)) &
                      +ZP(K)*(KW(1:3,1)-KW(1:3,5)) &
                +YP(J)*ZP(K)*(KW(1:3,4)-KW(1:3,1)-KW(1:3,8)+KW(1:3,5))

     X3(1:3)=KW(1:3,6)+YP(J)*(KW(1:3,7)-KW(1:3,6)) &
                      +ZP(K)*(KW(1:3,2)-KW(1:3,6)) &
                +YP(J)*ZP(K)*(KW(1:3,3)-KW(1:3,2)-KW(1:3,7)+KW(1:3,6))

     PT(1:3)=X2(1:3)-P0(1:3)
     DO ID=1,NID
      CALL SUB_01(ID,IDEC,P0,PT,XS)
      IF (IDEC.EQ.1) THEN
       X1(1:3)=XS(1:3)
       SL1(1:3,J,K)=XS(1:3)
       EXIT
      END IF
     END DO

     PRINT *,K,J,IDEC,YP(J),ZP(K)
     IF (IDEC.EQ.0) THEN
      PRINT *,K,J,YP(J),ZP(K)
      STOP'ERROR IN BLOCK 1/A'
     END IF

     PT(1:3)=X3(1:3)-P0(1:3)
     DO ID=1,NID
      CALL SUB_01(ID,IDEC,P0,PT,XS)
      IF (IDEC.EQ.1) THEN
       X4(1:3)=XS(1:3)
       SL2(1:3,J,K)=XS(1:3)
       EXIT
      END IF
     END DO

     PRINT *,K,J,IDEC
     IF (IDEC.EQ.0) STOP'ERROR IN BLOCK 1/B'

    DO I=1,N4
     IK=IK+1
     KN(1:3,IK)=X1(1:3)+REAL(I-1)*(X2(1:3)-X1(1:3))/REAL(N4)
    END DO

    DO I=1,N2+1
     IK=IK+1
     KN(1:3,IK)=X2(1:3)+XP(I)*(X3(1:3)-X2(1:3))
    END DO

    DO I=2,N4+1
     IK=IK+1
     KN(1:3,IK)=X3(1:3)+REAL(I-1)*(X4(1:3)-X3(1:3))/REAL(N4)
    END DO

   END DO
  END DO

! ***************************************

! BLOCK 2
  PRINT *,'Block 2'

  P0(1:3)=KW(1:3,9)

  DO J=1,N1+1
   DO I=1,N2+1

!    IF (YP(J).LT.1.0E0) P0(2)=YP(J)
!    IF (YP(J).GE.1.0E0) P0(2)=1.0E0

    X2(1:3)=KW(1:3,1)+XP(I)*(KW(1:3,2)-KW(1:3,1)) &
                     +YP(J)*(KW(1:3,4)-KW(1:3,1)) &
               +XP(I)*YP(J)*(KW(1:3,3)-KW(1:3,4)-KW(1:3,2)+KW(1:3,1))

    SB1(1:3,I,J)=X2(1:3)

    PT(1:3)=X2(1:3)-P0(1:3)
    DO ID=1,NID
     CALL SUB_01(ID,IDEC,P0,PT,XS)
     IF (IDEC.EQ.1) THEN
      SB2(1:3,I,J)=XS(1:3)
      EXIT
     END IF
    END DO
    IF (IDEC.EQ.0) STOP'ERROR IN BLOCK 2'

   END DO
  END DO

  DO K=2,N4+1
   DO J=1,N1+1
    DO I=2,N2
     IK=IK+1
     KN(1:3,IK)=SB1(1:3,I,J) &
               +DBLE(K-1)*(SB2(1:3,I,J)-SB1(1:3,I,J))/DBLE(N4)
    END DO
   END DO
  END DO

! ***************************************

! BLOCK 3
  PRINT *,'Block 3'

  P0(1:3)=KW(1:3,9)
!  P0(2)=1.0E0

  DO K=1,N3+1
   DO I=1,N2+1

     X2(1:3)=KW(1:3,8)+XP(I)*(KW(1:3,7)-KW(1:3,8)) &
                      +ZP(K)*(KW(1:3,4)-KW(1:3,8)) &
         +XP(I)*ZP(K)*(KW(1:3,3)-KW(1:3,4)-KW(1:3,7)+KW(1:3,8))

     SS1(1:3,I,K)=X2(1:3)

     PT(1:3)=X2(1:3)-P0(1:3)
     DO ID=1,NID
      CALL SUB_01(ID,IDEC,P0,PT,XS)
      IF (IDEC.EQ.1) THEN
       SS2(1:3,I,K)=XS(1:3)
       EXIT
      END IF
     END DO
     IF (IDEC.EQ.0) STOP'ERROR IN BLOCK 3'

     PRINT *,"I,K ",I,K,IDEC

    END DO
   END DO

  DO K=1,N3
   DO J=2,N4+1
    DO I=2,N2
     IK=IK+1
     KN(1:3,IK)=SS1(1:3,I,K) &
               +DBLE(J-1)*(SS2(1:3,I,K)-SS1(1:3,I,K))/DBLE(N4)
    END DO
   END DO
  END DO

! **********************************************

 OPEN(1,FILE='v_netz.dat',CARRIAGECONTROL='LIST',STATUS='UNKNOWN')

 WRITE(1,'(''  TITLE = "VERNETZUNG"'')')
 WRITE(1,'(''  VARIABLES = "X", "Y", "Z"'')')
 WRITE(1,'(''  ZONE N=29193, E=27200, F=FEPOINT, ET=BRICK'')')

   DO I=1,NK
    WRITE(1,*) KN(1,I),KN(2,I),KN(3,I)
   END DO
   DO IE=1,NE
    WRITE(1,'(I8,I8,I8,I8,I8,I8,I8,I8)') &
             INDE(1,IE),INDE(2,IE),INDE(3,IE),INDE(4,IE), &
             INDE(5,IE),INDE(6,IE),INDE(7,IE),INDE(8,IE)
   END DO

 CLOSE(1)

  OPEN(1,FILE='tecFL.dat',CARRIAGECONTROL='LIST',STATUS='UNKNOWN')
   WRITE(1,*)'TITLE = "LOCAL SOLIDIFICATION CONDITIONS"'
   WRITE(1,*)'VARIABLES = "X","Y","Z"'

   WRITE(1,*)'ZONE I=21, J=21, F=POINT'
   DO J=1,N1+1
    DO I=1,2*NX+1
    WRITE(1,*) SB2(1,I,J),SB2(2,I,J),SB2(3,I,J)
    END DO
   END DO
   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,2*NX+1
    WRITE(1,*) SS2(1,I,J),SS2(2,I,J),SS2(3,I,J)
    END DO
   END DO
   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,N1+1
    WRITE(1,*) SL1(1,I,J),SL1(2,I,J),SL1(3,I,J)
    END DO
   END DO
   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,N1+1
    WRITE(1,*) SL2(1,I,J),SL2(2,I,J),SL2(3,I,J)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,2*NX+1
     XDV(1:3)=(1.0E0-XP(I))*(1.0E0-ZP(J))*KW(1:3,4)+XP(I)*(1.0E0-ZP(J))*KW(1:3,3) &
         +XP(I)*ZP(J)*KW(1:3,7)+(1.0E0-XP(I))*ZP(J)*KW(1:3,8)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=21, F=POINT'
   DO J=1,N1+1
    DO I=1,2*NX+1
     XDV(1:3)=(1.0E0-XP(I))*(1.0E0-YP(J))*KW(1:3,1)+XP(I)*(1.0E0-YP(J))*KW(1:3,2) &
         +XP(I)*YP(J)*KW(1:3,3)+(1.0E0-XP(I))*YP(J)*KW(1:3,4)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,N1+1
     XDV(1:3)=(1.0E0-YP(I))*(1.0E0-ZP(J))*KW(1:3,1)+YP(I)*(1.0E0-ZP(J))*KW(1:3,4) &
         +YP(I)*ZP(J)*KW(1:3,8)+(1.0E0-YP(I))*ZP(J)*KW(1:3,5)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,N1+1
     XDV(1:3)=(1.0E0-YP(I))*(1.0E0-ZP(J))*KW(1:3,2)+YP(I)*(1.0E0-ZP(J))*KW(1:3,3) &
         +YP(I)*ZP(J)*KW(1:3,7)+(1.0E0-YP(I))*ZP(J)*KW(1:3,6)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

 CLOSE(1)

! *****************************************

  DO I=1,NESU
   IE=IESU(I)
   I1=INDE(5,IE); I2=INDE(6,IE); I3=INDE(7,IE); I4=INDE(8,IE);
   DO J=1,NS
    K=NRS(J)
    IF (K.EQ.I1) INDESU(1,I)=J
    IF (K.EQ.I2) INDESU(2,I)=J
    IF (K.EQ.I3) INDESU(3,I)=J
    IF (K.EQ.I4) INDESU(4,I)=J
   END DO
  END DO

  OPEN(1,FILE='tecs.dat',CARRIAGECONTROL='LIST',STATUS='UNKNOWN')
   WRITE(1,*)'TITLE = "LOCAL SOLIDIFICATION CONDITIONS"'
   WRITE(1,*)'VARIABLES = "X","Y","Z"'
   WRITE(1,'(''  ZONE N=1661, E=1600, F=FEPOINT, ET=QUADRILATERAL'')')

   DO I=1,NS
    WRITE(1,*) KN(1,NRS(I)),KN(2,NRS(I)),KN(3,NRS(I))
   END DO
   DO I=1,NESU
     WRITE(1,*) INDESU(1,I),INDESU(2,I),INDESU(3,I),INDESU(4,I)
   END DO

  CLOSE(1)

PRINT *,"  "
PRINT *," End Subr. MESH"
PRINT *,"  "

END SUBROUTINE MESH


  SUBROUTINE INDEX

! Das Feld INDE bestimmen
! ***********************

  IMPLICIT NONE

  INTEGER :: IS,IES,ISL,ISY,IHSL
  INTEGER :: I,J,K,IK,IE,I1


  PRINT *,"  "
  PRINT *," Start Subr. INDEX"

! ************ BLOCK 1 ***************

  I1=N4+N2+N4+1
  IK=0
  IS=0
  IES=0
  ISY=0
  ISL=0

  DO K=1,N3+1
   DO J=1,N1+1
    DO I=1,N4+N2+N4+1
     IK=IK+1
     BL1(I,J,K)=IK

    IF ((I.EQ.1).OR.(I.EQ.I1)) THEN
     ISL=ISL+1
     NRSL(ISL)=IK
    END IF

    IF (J.EQ.1) THEN
     ISY=ISY+1
     NRSY(ISY)=IK
    END IF

    IF (K.EQ.1) THEN
     IS=IS+1
     NRS(IS)=IK
    END IF

    END DO
   END DO
  END DO

! ************ BLOCK 2 ***************

  DO K=2,N4+1
   DO J=1,N1+1
    DO I=2,N2
     IK=IK+1
     BL2(I,J,K)=IK

     IF (K.EQ.N4+1) THEN
      ISL=ISL+1
      NRSL(ISL)=IK
     END IF

     IF (J.EQ.1) THEN
      ISY=ISY+1
      NRSY(ISY)=IK
     END IF

    END DO
   END DO
  END DO

   DO J=1,N1+1
    DO I=1,N2+1
     BL2(I,J,1)=BL1(N4+I,J,N3+1)
    END DO
   END DO

  DO K=1,N4+1
   DO J=1,N1+1
    BL2(1,J,K)=BL1(N4+2-K,J,N3+1)
   END DO
  END DO

  DO K=1,N4+1
   DO J=1,N1+1
    BL2(N2+1,J,K)=BL1(N4+N2+K,J,N3+1)
   END DO
  END DO

! ************ BLOCK 3 ***************

  DO K=1,N3
   DO J=2,N4+1
    DO I=2,N2
     IK=IK+1
     BL3(I,J,K)=IK

     IF (J.EQ.N4+1) THEN
      ISL=ISL+1
      NRSL(ISL)=IK
     END IF

     IF (K.EQ.1) THEN
      IS=IS+1
      NRS(IS)=IK
     END IF

    END DO
   END DO
  END DO

  IF (ISL.NE.NSL) THEN
   PRINT *," ISL.GT.NSL !!!!"
   STOP
  END IF

  IF (ISY.NE.NSY) THEN
   PRINT *," ISY.GT.NSY !!!!"
   STOP
  END IF

  IF (IS.NE.NS) THEN
   PRINT *," IS.GT.NS !!!!"
   STOP
  END IF

! Front

  DO K=1,N3+1
   DO I=1,N2+1
    BL3(I,1,K)=BL1(N4+I,N1+1,K)
   END DO
  END DO

! Linke Seite

  DO K=1,N3+1
   DO J=1,N4+1
    BL3(1,J,K)=BL1(N4+2-J,N1+1,K)
   END DO
  END DO

! Rechte Seite

  DO K=1,N3+1
   DO J=1,N4+1
    BL3(N2+1,J,K)=BL1(N4+N2+J,N1+1,K)
   END DO
  END DO

! Boden

  DO J=1,N4+1
   DO I=1,N2+1
    BL3(I,J,N3+1)=BL2(I,N1+1,J)
   END DO
  END DO

! Besetzung der Indexvektoren und der Oberflaechenelemente

  IE=0
  IES=0
  IHSL=0

! ************ BLOCK 1 ***************

  DO K=1,N3
   DO J=1,N1
    DO I=1,N4+N2+N4

     IE=IE+1
     INDE(1,IE)=BL1(I  ,J  ,K+1)
     INDE(2,IE)=BL1(I+1,J  ,K+1)
     INDE(3,IE)=BL1(I+1,J+1,K+1)
     INDE(4,IE)=BL1(I  ,J+1,K+1)
     INDE(5,IE)=BL1(I  ,J  ,K  )
     INDE(6,IE)=BL1(I+1,J  ,K  )
     INDE(7,IE)=BL1(I+1,J+1,K  )
     INDE(8,IE)=BL1(I  ,J+1,K  )

     IF (K.EQ.1) THEN
      IES=IES+1
      IESU(IES)=IE
     END IF

     IF (I.EQ.1) THEN
      IHSL=IHSL+1
      IESL(1,IHSL)=IE
      IESL(2,IHSL)=4
     END IF

     IF (I.EQ.N4+N2+N4) THEN
      IHSL=IHSL+1
      IESL(1,IHSL)=IE
      IESL(2,IHSL)=2
     END IF

    END DO
   END DO
  END DO

! ************ BLOCK 2 ***************

  DO K=1,N4
   DO J=1,N1
    DO I=1,N2
     IE=IE+1
     INDE(1,IE)=BL2(I  ,J  ,K+1)
     INDE(2,IE)=BL2(I+1,J  ,K+1)
     INDE(3,IE)=BL2(I+1,J+1,K+1)
     INDE(4,IE)=BL2(I  ,J+1,K+1)
     INDE(5,IE)=BL2(I  ,J  ,K  )
     INDE(6,IE)=BL2(I+1,J  ,K  )
     INDE(7,IE)=BL2(I+1,J+1,K  )
     INDE(8,IE)=BL2(I  ,J+1,K  )

     IF (K.EQ.N4) THEN
      IHSL=IHSL+1
      IESL(1,IHSL)=IE
      IESL(2,IHSL)=5
     END IF

    END DO
   END DO
  END DO

! ************ BLOCK 3 ***************

  DO K=1,N3
   DO J=1,N4
    DO I=1,N2
     IE=IE+1
     INDE(1,IE)=BL3(I  ,J  ,K+1)
     INDE(2,IE)=BL3(I+1,J  ,K+1)
     INDE(3,IE)=BL3(I+1,J+1,K+1)
     INDE(4,IE)=BL3(I  ,J+1,K+1)
     INDE(5,IE)=BL3(I  ,J  ,K  )
     INDE(6,IE)=BL3(I+1,J  ,K  )
     INDE(7,IE)=BL3(I+1,J+1,K  )
     INDE(8,IE)=BL3(I  ,J+1,K  )
     IF (IE.GT.NE) STOP

     IF (K.EQ.1) THEN
      IES=IES+1
      IESU(IES)=IE
     END IF

     IF (J.EQ.N4) THEN
      IHSL=IHSL+1
      IESL(1,IHSL)=IE
      IESL(2,IHSL)=3
     END IF

    END DO
   END DO
  END DO

  IF (IES.NE.NESU) THEN
   PRINT *," IES.NE.NESU !!!!"
   STOP
  END IF

  IF (IHSL.NE.NESL) THEN
   PRINT *," IESL.NE.NESL !!!!"
   STOP
  END IF

  ! *****************************************

  PRINT *,"  "
  PRINT *," End Subr. INDEX"
  PRINT *,"  "

END SUBROUTINE INDEX



  SUBROUTINE DATIN

! Knotenpunkte und deren Zusammengehoerigkeit aus interface.dat
! einlesen und Knotenpunkte des Grundwuerfels festlegen
! *************************************************************

  IMPLICIT NONE

  REAL, DIMENSION (1:3) :: XDV
  REAL :: XMAX,YMAX,ZMAX,DX,DY,DZ
  REAL :: DXX,DYY,DZZ
  REAL :: XORIG,YORIG,ZORIG,XDUM

  INTEGER :: I,J,K,II

  CHARACTER (LEN=2) :: CDUM

  PRINT *,"  "
  PRINT *," Start Subr. DATIN"

! ***************************************
!   Diskretisierung des Einheitswürfels

  FX=0.99E0
  FY=0.99E0
  FZ=1.01E0

  PRINT *,"  "
  PRINT *," FX = ",FX
  PRINT *," FY = ",FY
  PRINT *," FZ = ",FZ
  PRINT *,"  "

! ***************************************

  DXX=0.5E0*(1.E0-FX)/(1.E0-FX**NX)
  XP(NX+1)=0.5E0
  DO I=NX+2,2*NX+1
   XP(I)=XP(I-1)+DXX
   DXX=DXX*FX
  END DO

  DO I=1,NX
   XP(I)=1.E0-XP(2*NX+2-I)
  END DO

  XP(1)=0.0E0

  PRINT *," "
  DO I=1,2*NX+1
   PRINT *," XP: ",I,XP(I)
  END DO
  PRINT *," "

! ***************************************

  DYY=1.0E0*(1.E0-FY)/(1.E0-FY**N1)
  YP(1)=0.0E0
  DO I=2,N1+1
   YP(I)=YP(I-1)+DYY
   DYY=DYY*FY
  END DO

  PRINT *," "
  DO I=1,N1+1
   PRINT *," YP: ",I,YP(I)
  END DO
  PRINT *," "

! ***************************************

  DZZ=1.0E0*(1.E0-FZ)/(1.E0-FZ**N3)
  ZP(1)=0.0E0
  DO I=2,N3+1
   ZP(I)=ZP(I-1)+DZZ
   DZZ=DZZ*FZ
  END DO

  PRINT *," "
  DO I=1,N3+1
   PRINT *," ZP: ",I,ZP(I)
  END DO
  PRINT *," "

  XP(1)=0.0E0; XP(2*NX+1)=1.0E0
  YP(1)=0.0E0; YP(N1+1  )=1.0E0
  ZP(1)=0.0E0; ZP(N3+1  )=1.0E0

! **************************************************************

  OPEN(1,FILE='interface.dat',FORM='FORMATTED',STATUS='UNKNOWN')

  DO I=1,3
   READ(1,'(A2)') CDUM
  END DO

  DO I=1,NKD
   READ(1,*) KND(1,I),KND(2,I),KND(3,I)
  END DO

  DO I=1,NID
   READ(1,*) DINDE(1,I),DINDE(2,I),DINDE(3,I)
  END DO

  CLOSE(1)

  XORIG=1.E+6; XMAX=-1.E+6;
  YORIG=1.E+6; YMAX=-1.E+6;
  ZORIG=1.E+6; ZMAX=-1.E+6;

  DO I=1,NKD
   IF (KND(1,I).LT.XORIG) XORIG=KND(1,I)
   IF (KND(1,I).GT.XMAX ) XMAX =KND(1,I)
   IF (KND(2,I).LT.YORIG) YORIG=KND(2,I)
   IF (KND(2,I).GT.YMAX ) YMAX =KND(2,I)
   IF (KND(3,I).LT.ZORIG) ZORIG=KND(3,I)
   IF (KND(3,I).GT.ZMAX ) ZMAX =KND(3,I)
  END DO

  YORIG=0.0E0
  PRINT *," XORIG= ",XORIG,"XMAX=",XMAX
  PRINT *," YORIG= ",YORIG,"YMAX=",YMAX
  PRINT *," ZORIG= ",ZORIG,"ZMAX=",ZMAX

  PRINT *," "
  PRINT *,"Knoten des Grundwuerfels wird festgelegt !"
  PRINT *," "

  DX=.10*(XMAX-XORIG)
  DY=.10*(YMAX-YORIG)
  DZ=.65*(ZMAX-ZORIG)

  KW(2,1)=0.0E0;        KW(1,1)=XORIG+3.0*DX; KW(3,1)=ZORIG+DZ;
  KW(2,2)=0.0E0;        KW(1,2)=XORIG+7.0*DX; KW(3,2)=ZORIG+DZ;
  KW(2,3)=YORIG+3.0*DY; KW(1,3)=XORIG+6.0*DX; KW(3,3)=ZORIG+DZ;
  KW(2,4)=YORIG+3.0*DY; KW(1,4)=XORIG+4.0*DX; KW(3,4)=ZORIG+DZ;

  KW(2,5)=0.0E0;        KW(1,5)=XORIG+2.0*DX; KW(3,5)=ZMAX;
  KW(2,6)=0.0E0;        KW(1,6)=XORIG+8.0*DX; KW(3,6)=ZMAX;
  KW(2,7)=YORIG+4.5*DY; KW(1,7)=XORIG+7.0*DX; KW(3,7)=ZMAX;
  KW(2,8)=YORIG+4.5*DY; KW(1,8)=XORIG+3.0*DX; KW(3,8)=ZMAX;

  KW(2,9)=0.0E0; KW(1,9)=.5E0*(XORIG+XMAX); KW(3,9)=ZMAX;

  OPEN(1,FILE='tec.dat',CARRIAGECONTROL='LIST',STATUS='UNKNOWN')
   WRITE(1,*)'TITLE = "LOCAL SOLIDIFICATION CONDITIONS"'
   WRITE(1,*)'VARIABLES = "X","Y","Z"'
   WRITE(1,*)'ZONE F=FEPOINT,ET=TRIANGLE,N=', NKD, ',E=',NID
   DO I=1,NKD
    WRITE(1,*) KND(1,I),KND(2,I),KND(3,I)
   END DO
   DO I=1,NID
    WRITE(1,*) DINDE(1,I),DINDE(2,I),DINDE(3,I)
   END DO

   WRITE(1,*)'VARIABLES = "X","Y","Z"'

   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,2*NX+1
     XDV(1:3)=(1.0E0-XP(I))*(1.0E0-ZP(J))*KW(1:3,4)+XP(I)*(1.0E0-ZP(J))*KW(1:3,3) &
         +XP(I)*ZP(J)*KW(1:3,7)+(1.0E0-XP(I))*ZP(J)*KW(1:3,8)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=21, F=POINT'
   DO J=1,N1+1
    DO I=1,2*NX+1
     XDV(1:3)=(1.0E0-XP(I))*(1.0E0-YP(J))*KW(1:3,1)+XP(I)*(1.0E0-YP(J))*KW(1:3,2) &
         +XP(I)*YP(J)*KW(1:3,3)+(1.0E0-XP(I))*YP(J)*KW(1:3,4)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,N1+1
     XDV(1:3)=(1.0E0-YP(I))*(1.0E0-ZP(J))*KW(1:3,1)+YP(I)*(1.0E0-ZP(J))*KW(1:3,4) &
         +YP(I)*ZP(J)*KW(1:3,8)+(1.0E0-YP(I))*ZP(J)*KW(1:3,5)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,*)'ZONE I=21, J=13, F=POINT'
   DO J=1,N3+1
    DO I=1,N1+1
     XDV(1:3)=(1.0E0-YP(I))*(1.0E0-ZP(J))*KW(1:3,2)+YP(I)*(1.0E0-ZP(J))*KW(1:3,3) &
         +YP(I)*ZP(J)*KW(1:3,7)+(1.0E0-YP(I))*ZP(J)*KW(1:3,6)
    WRITE(1,*) XDV(1),XDV(2),XDV(3)
    END DO
   END DO

   WRITE(1,'(''  ZONE N=9, E=1, F=FEPOINT, ET=BRICK'')')
     DO I=1,9
    WRITE(1,*) KW(1,I),KW(2,I),KW(3,I)
   END DO
   DO I=1,1
    WRITE(1,'(I8,I8,I8,I8,I8,I8,I8,I8)') &
             1,2,3,4,5,6,7,8
   END DO

 CLOSE(1)

PRINT *,"  "
PRINT *," End Subr. DATIN"
PRINT *,"  "

END SUBROUTINE DATIN


END PROGRAM MESHV
