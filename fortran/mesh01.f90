PROGRAM MESHMARC

IMPLICIT NONE

INTEGER, PARAMETER :: NE2D=8944, NK2D=9125, NZ=25
INTEGER, PARAMETER :: NE=NZ*NE2D, NK=(NZ+1)*NK2D
INTEGER, PARAMETER :: IDM=39,IDCK=20

INTEGER, DIMENSION (1:7,1:IDCK,1:NK) :: INDK
INTEGER, DIMENSION (1:IDM,1:NK) :: INDM
INTEGER, DIMENSION (1:8,1:NE) :: INDE
INTEGER, DIMENSION (1:4,1:NE) :: INDESU,INDESU2D
INTEGER, DIMENSION (1:NK) :: ICL,ICM,ICK,ICSU
REAL, DIMENSION (1:3,1:NK) :: KN
REAL, DIMENSION (0:NZ) :: ZP

INTEGER, DIMENSION (1:4,1:NE2D) :: INDE2D
INTEGER, DIMENSION (1:NE2D) :: IBOUND
INTEGER, DIMENSION (1:4,1:6) :: FACES
INTEGER, DIMENSION (1:4) :: IFACE,IFACEH
DOUBLE PRECISION, DIMENSION (1:3,1:NK2D) :: KN2D

CHARACTER (LEN=1) :: CDUM
CHARACTER (LEN=60) :: CD60
CHARACTER (LEN=80) :: CD80

REAL :: DZ,FACX,FACZ,XM,YM,XDUM,YDUM,xmin,xmax,ymin,ymax
INTEGER :: I,J,K,L,IE,I1,I2,I3,I4,IMIN,JMIN,II,JJ,IDEC,ICKMAX,NESU,NKSU

PRINT *,' '
PRINT *,' NK2D,NE2D =',NK2D,NE2D
PRINT *,' NK,  NE   =',NK,NE
PRINT *,' '

! ***************************************************************
!     i=5707                            i=628, (25.5,9.0) 
!        .__________________________________.
!
!               Edge: 0.125 mm
!
!        .__________________________________.
!     i=15683 (0.0,0.0)                 i=10733 (25.0,0.0)  
!
! ***************************************************************

   FACX=1.0E0 ! Edge=Edge*facx
   i1=6952;
   i2=2203;
   i3=277;
   i4=4847;

   OPEN(1,FILE='Model01_flach.bdf',FORM='FORMATTED',STATUS='UNKNOWN')
    DO I=1,8
     READ(1,'(A1)') CDUM
    END DO
    DO I=1,NE2D
     READ(1,'(A60)') CD60
     READ(CD60(21:60),*) INDE2D(1:4,I)
    END DO
    READ(1,'(A1)') CDUM
    READ(1,'(A1)') CDUM
    READ(1,'(A1)') CDUM

    DO I=1,NK2D
     READ(1,'(A80)') CD80
     READ(CD80(40:80),*) KN2D(1:2,I)
     READ(1,'(A1)') CDUM
     KN2D(3,I)=0.0E0

     KN2D(1,I)=KN2D(1,I)*FACX
     KN2D(2,I)=KN2D(2,I)*FACX
    END DO
   CLOSE(1)

   OPEN(1,FILE='tec_2D.dat',FORM='FORMATTED',STATUS='UNKNOWN')

    WRITE(1,'(''  TITLE = "Laserpolieren"'')')
    WRITE(1,'(''  VARIABLES = "X", "Y"'')')
    WRITE(1,*)'ZONE F=FEPOINT,ET=QUADRILATERAL,N=',NK2D,',E=',NE2D

    DO I=1,NK2D
     WRITE(1,*) KN2D(1,I),KN2D(2,I)
    END DO

    DO I=1,NE2D
     WRITE(1,'(I8,I8,I8,I8)') &
              INDE2D(1,I),INDE2D(2,I),INDE2D(3,I),INDE2D(4,I)
    END DO

   CLOSE(1)
   
   xmin=KN2D(1,i1)
   xmax=KN2D(1,i2)
   ymin=KN2D(2,i1)
   ymax=KN2D(2,i4)
   
   PRINT *,' '
   PRINT *,' xmin,xmax:',xmin/facx,xmax/facx
   PRINT *,' ymin,ymax:',ymin/facx,ymax/facx
   PRINT *,' '
   
   IBOUND=0
   DO IE=1,NE2D
    xm=0.25E0*(KN2D(1,INDE2D(1,IE))+KN2D(1,INDE2D(2,IE)) &
              +KN2D(1,INDE2D(3,IE))+KN2D(1,INDE2D(4,IE)));
    ym=0.25E0*(KN2D(2,INDE2D(1,IE))+KN2D(2,INDE2D(2,IE)) &
              +KN2D(2,INDE2D(3,IE))+KN2D(2,INDE2D(4,IE)));
	IF ((xmin.LT.xm).and.(xm.LT.xmax)) THEN
	  IF ((ymin.LT.ym).and.(ym.LT.ymax)) THEN
	    IBOUND(IE)=1;	
!	    PRINT *,IE,XDUM,YDUM
	  END IF	
	END IF
   END DO
   OPEN(1,FILE='Bound01.dat',FORM='FORMATTED',STATUS='UNKNOWN')
     DO IE=1,NE2D
       IF (IBOUND(IE).EQ.1) WRITE(1,*) (NZ-1)*NE2D+IE,6
     END DO
   CLOSE(1)
   
   PRINT *,'Bound01.dat rausgeschrieben'
   !read(*,*) i
   !stop   
   
! ***************************************************************

! Setze die Schichthoehen

 FACZ=1.23E0
 DZ=0.00125E0
 ZP(0)=0.000E0

 DO I=1,NZ
  ZP(I)=ZP(I-1)+DZ
  IF (I.GT.5) DZ=FACZ*DZ
 END DO

 DO I=1,NZ
  ZP(I)=-ZP(I)
 END DO

 DO I=0,NZ
  PRINT *,I,ZP(I)
 END DO
 read(*,*) i
 
 K=0
 DO J=1,NZ
  DO I=1,NE2D
   K=K+1
   INDE(1,K)=INDE2D(1,I)+(J-1)*NK2D
   INDE(2,K)=INDE2D(2,I)+(J-1)*NK2D
   INDE(3,K)=INDE2D(3,I)+(J-1)*NK2D
   INDE(4,K)=INDE2D(4,I)+(J-1)*NK2D
   INDE(5,K)=INDE(1,K)+NK2D
   INDE(6,K)=INDE(2,K)+NK2D
   INDE(7,K)=INDE(3,K)+NK2D
   INDE(8,K)=INDE(4,K)+NK2D
  END DO
 END DO

 K=0
 DO J=NZ,0,-1
  DO I=1,NK2D
   K=K+1
   KN(1,K)=KN2D(1,I)
   KN(2,K)=KN2D(2,I)
   KN(3,K)=ZP(J)
  END DO
 END DO

 PRINT *,' '

! *************************************************************************
! Knotenpunkte der 6 W�rfelfl�chen, Front, rechts, back, links, bottom, top


FACES(1,1)=1; FACES(2,1)=2; FACES(3,1)=6; FACES(4,1)=5;    
FACES(1,2)=2; FACES(2,2)=3; FACES(3,2)=7; FACES(4,2)=6;    
FACES(1,3)=3; FACES(2,3)=4; FACES(3,3)=8; FACES(4,3)=7;    
FACES(1,4)=4; FACES(2,4)=1; FACES(3,4)=5; FACES(4,4)=8;    
FACES(1,5)=1; FACES(2,5)=4; FACES(3,5)=3; FACES(4,5)=2;    
FACES(1,6)=5; FACES(2,6)=6; FACES(3,6)=7; FACES(4,6)=8;    

ICK(1:NK)=0
INDK(1:7,1:IDCK,1:NK)=0

PRINT *,' Oberfl�che ableiten!'

DO IE=1,NE
 DO J=1,6
  IFACE(1)=INDE(FACES(1,J),IE);  IFACE(2)=INDE(FACES(2,J),IE); 
  IFACE(3)=INDE(FACES(3,J),IE);  IFACE(4)=INDE(FACES(4,J),IE); 
  
  ! Die Knotennummern pro Fl�che der Gr��e nach sortieren
  DO I=1,4
   IMIN=NK+1
   DO K=1,4
    IF (IFACE(K).LT.IMIN) THEN
	 IMIN=IFACE(K)
	 JMIN=K
	END IF
   END DO
   IFACEH(I)=IFACE(JMIN)
   IFACE(JMIN)=NK+1
  END DO
  IFACE(1:4)=IFACEH(1:4)
  
  IF (IE.GT.NE-10) PRINT *,IE,IFACE(1:4)
  
  II=IFACE(1)
  
  IF (ICK(II).GT.0) THEN
   IDEC=1 
   DO I=1,ICK(II)
    IF ((INDK(1,I,II).EQ.IFACE(1)).AND.(INDK(2,I,II).EQ.IFACE(2)).AND. &
	    (INDK(3,I,II).EQ.IFACE(3)).AND.(INDK(4,I,II).EQ.IFACE(4))) THEN
	 IDEC=0
	 INDK(7,I,II)=1
	 EXIT
	END IF
   END DO
   IF (IDEC.EQ.1) THEN
    ICK(II)=ICK(II)+1
	K=ICK(II)
    INDK(1:4,K,II)=IFACE(1:4); INDK(5,K,II)=IE; INDK(6,K,II)=J; INDK(7,K,II)=0;   
   END IF
  END IF
  
  IF (ICK(II).EQ.0) THEN
   ICK(II)=1
   INDK(1:4,1,II)=IFACE(1:4); INDK(5,1,II)=IE; INDK(6,1,II)=J; INDK(7,1,II)=0;
   ! Knotennummern            Elementnummer    Fl�chennummer   gleich 0 bedeutet, bisher nur einmal aufgetreten
  END IF

 END DO ! J
END DO ! IE

ICKMAX=0
NESU=0
DO I=1,NK
 IF (ICK(I).GT.ICKMAX) ICKMAX=ICK(I)
 DO J=1,IDCK
  IF ((INDK(5,J,I).NE.0).AND.(INDK(7,J,I).EQ.0)) NESU=NESU+1
 END DO
END DO

ICSU(1:NK)=0
JJ=0
DO I=1,NK
 DO J=1,ICK(I)
  IF ((INDK(5,J,I).NE.0).AND.(INDK(7,J,I).EQ.0)) THEN
   IE=INDK(5,J,I)
   K=INDK(6,J,I)
   JJ=JJ+1
   IFACE(1)=INDE(FACES(1,K),IE);  IFACE(2)=INDE(FACES(2,K),IE); 
   IFACE(3)=INDE(FACES(3,K),IE);  IFACE(4)=INDE(FACES(4,K),IE); 
   INDESU(1:4,JJ)=IFACE(1:4)
   
   ICSU(IFACE(1))=1
   ICSU(IFACE(2))=1
   ICSU(IFACE(3))=1
   ICSU(IFACE(4))=1
  END IF
 END DO
END DO

NKSU=0
DO I=1,NK
 IF (ICSU(I).NE.0) THEN
  NKSU=NKSU+1
  ICSU(I)=NKSU
 END IF
END DO

DO IE=1,NESU
 DO J=1,4
  II=INDESU(J,IE)
  INDESU2D(J,IE)=ICSU(II)
 END DO
END DO

PRINT *,'ICKMAX:',ICKMAX
PRINT *,'NESU:  ',NESU
PRINT *,'NKSU:  ',NKSU

 read(*,*) i

 OPEN(1,FILE='ICSU.dat',FORM='FORMATTED',STATUS='UNKNOWN')
   DO I=1,NK
    IF (ICSU(I).NE.0) WRITE(1,*) I
   END DO
 CLOSE(1)
 OPEN(1,FILE='INDESU.dat',FORM='FORMATTED',STATUS='UNKNOWN')
   DO IE=1,NESU
    WRITE(1,*) INDESU2D(1:4,IE)
   END DO
 CLOSE(1)

 OPEN(1,FILE='tec_Surf.dat',FORM='FORMATTED',STATUS='UNKNOWN')

 WRITE(1,'(''  TITLE = "Laserpolieren"'')')
 WRITE(1,'(''  VARIABLES = "X", "Y", "Z"'')')
 WRITE(1,*)'ZONE F=FEPOINT,ET=QUADRILATERAL,N=',NKSU,',E=',NESU

   DO I=1,NK
    IF (ICSU(I).NE.0) WRITE(1,*) KN(1,I),KN(2,I),KN(3,I)
   END DO

   DO IE=1,NESU
    WRITE(1,*) INDESU2D(1:4,IE)
   END DO
   
 CLOSE(1)

!stop

! ***************************************************************

 CALL INDEX

 PRINT *,'Rausschreiben'

 OPEN(1,FILE='tec_test_For.dat',FORM='FORMATTED',STATUS='UNKNOWN')

 WRITE(1,'(''  TITLE = "Laserpolieren"'')')
 WRITE(1,'(''  VARIABLES = "X", "Y", "Z"'')')
 WRITE(1,*)'ZONE F=FEPOINT,ET=BRICK,N=',NK,',E=',NE

   PRINT *,'OKAY'
   DO I=1,NK
    WRITE(1,*) KN(1,I),KN(2,I),KN(3,I)
   END DO

   DO I=1,NE
    WRITE(1,'(I8,I8,I8,I8,I8,I8,I8,I8)') &
             INDE(1,I),INDE(2,I),INDE(3,I),INDE(4,I), &
             INDE(5,I),INDE(6,I),INDE(7,I),INDE(8,I)
   END DO

 CLOSE(1)


CONTAINS

! ***************************************************************
!                       Next Subroutine
! ***************************************************************


SUBROUTINE INDEX

INTEGER :: L,IP,IZ,IMIN,IMAX,IW
INTEGER, DIMENSION(IDM) :: IB

! Berechne INDM
! *************

DO I=1,NK
 DO J=1,IDM
  INDM(J,I)=0
 END DO
 INDM(1,I)=I
END DO

DO I=1,NE

 DO J=1,8
  IB(J)=INDE(J,I)
 END DO

 DO J=1,8
  IZ=IB(J)
  DO K=1,8
   IP=0
   DO L=1,IDM
    IF (INDM(L,IZ).EQ.IB(K)) IP=1
   END DO
   IF (IP.EQ.0) THEN
    DO L=IDM,2,-1
     IF ((INDM(L,IZ).EQ.0).AND.(INDM(L-1,IZ).NE.0)) INDM(L,IZ)=IB(K)
    END DO
   END IF
  END DO
 END DO

END DO

! Sortieren
! *********

DO I=1,NK

DO J=1,IDM
 IB(J)=INDM(J,I)
 INDM(J,I)=0
END DO

DO J=1,IDM
 IMIN=NK+1

 DO K=1,IDM
  IF ((IB(K).NE.0).AND.(IB(K).LT.IMIN)) THEN
   IMIN=IB(K)
   IP=K
  END IF
 END DO
 IB(IP)=0
 IF (IMIN.LT.NK+1) INDM(J,I)=IMIN
 END DO
END DO

IMAX=0
DO I=1,NK
 ICL(I)=0
 DO J=1,IDM
  IF (INDM(J,I).NE.0) ICL(I)=ICL(I)+1
 END DO
 IF (ICL(I).GT.IMAX) IMAX=ICL(I)
END DO

PRINT *," IMAX = ",IMAX
PRINT *," IDM  = ",IDM

IW=1
IF (IW.EQ.1) THEN
OPEN(1,FILE='Nodes.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
 WRITE(1) ((KN(J,I), J=1,3),I=1,NK)
CLOSE(1)

OPEN(1,FILE='INDE.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
 WRITE(1) ((INDE(J,I), J=1,8),I=1,NE)
CLOSE(1)

OPEN(1,FILE='INDM.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
 WRITE(1) ((INDM(J,I), J=1,IDM),I=1,NK)
CLOSE(1)

OPEN(1,FILE='ICL.DAT',FORM='UNFORMATTED',STATUS='UNKNOWN')
 WRITE(1) (ICL(I), I=1,NK)
CLOSE(1)
END IF

IW=0
IF (IW.EQ.1) THEN
OPEN(1,FILE='Nodes.dat',FORM='FORMATTED',STATUS='UNKNOWN')
 DO I=1,NK
  WRITE(1,*) KN(1:3,I)
 END DO
CLOSE(1)

OPEN(1,FILE='INDE.dat',FORM='FORMATTED',STATUS='UNKNOWN')
  DO I=1,NE
   WRITE(1,'(I8,I8,I8,I8,I8,I8,I8,I8)') INDE(1:8,I)
  END DO
CLOSE(1)

OPEN(1,FILE='INDM.dat',FORM='FORMATTED',STATUS='UNKNOWN')
  DO I=1,NK
   WRITE(1,'(I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8, &
             I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8, &
             I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8,I8)') INDM(1:IDM,I)
  END DO
CLOSE(1)

OPEN(1,FILE='ICL.dat',FORM='FORMATTED',STATUS='UNKNOWN')
 DO I=1,NK
  WRITE(1,*) ICL(I)
 END DO
CLOSE(1)
END IF

END SUBROUTINE INDEX


END PROGRAM MESHMARC
